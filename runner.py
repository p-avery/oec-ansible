#!/usr/bin/python

import argparse
import json
import logging
import sys
import os


parser = argparse.ArgumentParser()
parser.add_argument('-payload', '--payload', help='Payload from queue', required=True)
parser.add_argument('-apiKey', '--apiKey', help='The apiKey of the integration', required=True)
parser.add_argument('-opsgenieUrl', '--opsgenieUrl', help='The url', required=True)
parser.add_argument('-logLevel', '--logLevel', help='Level of log', required=True)

args = vars(parser.parse_args())

logging.basicConfig(stream=sys.stdout, level=args['logLevel'])

queue_message_string = args['payload']
queue_message = json.loads(queue_message_string)
api_key = args['apiKey']

def parse_from_details(key):
    if key in alert_from_opsgenie["details"].keys():
        return alert_from_opsgenie["details"][key]
    return ""

alert_id = queue_message["alert"]["alertId"]
entity = queue_message["alert"]['entity']

alert_api_url = args['opsgenieUrl'] + "/v2/alerts/" + alert_id + "/details?identifierType=id"
ansible_dir = "/tmp/" + alert_id

import ansible_runner
r = ansible_runner.run_async(private_data_dir='/tmp/demo', host_pattern=entity, playbook='facts.yml', extravars={'alert_api_url': alert_api_url, 'api_key': api_key,})
print("{}: {}".format(r.status, r.rc))
# successful: 0
for each_host_event in r.events:
    print(each_host_event['event'])
print("Final status:")
print(r.stats)
os.remove("/tmp/demo/env/extravars")